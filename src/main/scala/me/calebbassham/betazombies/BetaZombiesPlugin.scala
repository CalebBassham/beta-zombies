package me.calebbassham.betazombies

import me.calebbassham.scenariomanager.api.ScenarioManagerInstance
import org.bukkit.plugin.java.JavaPlugin

class BetaZombiesPlugin extends JavaPlugin {

  override def onEnable(): Unit = {
    ScenarioManagerInstance.getScenarioManager.register(new BetaZombies(), this)
  }

}
