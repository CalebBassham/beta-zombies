package me.calebbassham.betazombies

import java.util.concurrent.ThreadLocalRandom

import me.calebbassham.scenariomanager.api.SimpleScenario
import org.bukkit.Material
import org.bukkit.entity.Zombie
import org.bukkit.event.entity.EntityDeathEvent
import org.bukkit.event.{EventHandler, Listener}
import org.bukkit.inventory.ItemStack

class BetaZombies extends SimpleScenario() with Listener {

  @EventHandler
  def onZombieDeath(e: EntityDeathEvent): Unit = {
    val entity = e.getEntity
    if (!entity.isInstanceOf[Zombie]) return

    val world = entity.getWorld
    if (!scenarioManager.getGameWorldProvider.isGameWorld(world)) return

    e.getDrops.clear()

    val feathers = new ItemStack(Material.FEATHER, randInt(0, 2))
    e.getDrops.add(feathers)
  }

  private def randInt(min: Int, max: Int): Int = ThreadLocalRandom.current().nextInt(min, max + 1)

}
